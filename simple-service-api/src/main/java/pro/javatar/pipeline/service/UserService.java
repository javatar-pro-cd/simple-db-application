package pro.javatar.pipeline.service;


import pro.javatar.pipeline.service.model.UserBO;
import pro.javatar.pipeline.service.exception.UserNotFoundServiceException;

import java.util.List;

public interface UserService {
    UserBO findById(Long id) throws UserNotFoundServiceException;
    UserBO findByLogin(String login) throws UserNotFoundServiceException;
    UserBO findByEmail(String email) throws UserNotFoundServiceException;
    UserBO save(UserBO user);
    void delete(Long id);
    List<UserBO> findAll();
}