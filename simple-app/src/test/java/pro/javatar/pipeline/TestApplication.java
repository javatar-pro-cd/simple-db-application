package pro.javatar.pipeline;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;


@SpringBootApplication(scanBasePackages = "pro.javatar.pipeline")
@ActiveProfiles("test")
public class TestApplication {
}
