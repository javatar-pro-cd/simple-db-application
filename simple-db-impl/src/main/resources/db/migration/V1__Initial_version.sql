CREATE SEQUENCE users_id_seq;

CREATE TABLE users (
  id bigint NOT NULL DEFAULT nextval('users_id_seq'),
  login varchar(100) NOT NULL,
  email varchar(120) NOT NULL,
  first_name varchar(50) NOT NULL,
  last_name varchar(50) DEFAULT NULL,
  sex varchar(10) DEFAULT NULL,
  PRIMARY KEY (id),
  constraint UK_login UNIQUE (login),
  constraint UK_email UNIQUE (email)
);

ALTER SEQUENCE users_id_seq
OWNED BY users.id;
