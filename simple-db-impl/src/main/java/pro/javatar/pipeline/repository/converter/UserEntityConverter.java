package pro.javatar.pipeline.repository.converter;

import org.mapstruct.Mapper;
import pro.javatar.pipeline.repository.model.UserEntity;
import pro.javatar.pipeline.repository.model.UserPO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserEntityConverter {

    UserPO toUserPO(UserEntity entity);

    UserEntity toUserEntity(UserPO po);

    List<UserPO> toUserPOList(List<UserEntity> list);
}
