package pro.javatar.pipeline.repository.impl;

import org.springframework.stereotype.Component;
import pro.javatar.pipeline.repository.UserJpaRepository;
import pro.javatar.pipeline.repository.UserRepository;
import pro.javatar.pipeline.repository.model.UserEntity;
import pro.javatar.pipeline.repository.model.UserPO;
import pro.javatar.pipeline.repository.exception.UserNotFoundDBException;
import pro.javatar.pipeline.repository.converter.UserEntityConverter;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserRepositoryImpl implements UserRepository {

    private final UserJpaRepository repository;

    private final UserEntityConverter converter;

    public UserRepositoryImpl(UserJpaRepository repository, UserEntityConverter converter) {
        this.repository = repository;
        this.converter = converter;
    }

    @Override
    public UserPO findById(Long id) throws UserNotFoundDBException {
        return repository.findById(id)
                       .map(converter::toUserPO)
                       .orElseThrow(() -> new UserNotFoundDBException("User with id=" + id + " not found"));
    }

    @Override
    public UserPO findByLogin(String login) throws UserNotFoundDBException {
        return repository.findByLogin(login)
                       .map(converter::toUserPO)
                       .orElseThrow(() -> new UserNotFoundDBException("User with login=" + login + " not found"));
    }

    @Override
    public UserPO findByEmail(String email) throws UserNotFoundDBException {
        return repository.findByEmail(email)
                       .map(converter::toUserPO)
                       .orElseThrow(() -> new UserNotFoundDBException("User with email=" + email + " not found"));
    }

    @Override
    public UserPO save(UserPO user) {
        UserEntity entity = converter.toUserEntity(user);
        UserEntity saved = repository.save(entity);
        return converter.toUserPO(saved);
    }

    @Override
    public List<UserPO> findAll() {
        List<UserEntity> list = new ArrayList<>();
        repository.findAll().forEach(list::add);
        return converter.toUserPOList(list);
    }

    @Override
    public void delete(Long userId) {
        repository.deleteById(userId);
    }
}
