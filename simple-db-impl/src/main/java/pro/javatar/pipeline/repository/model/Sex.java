package pro.javatar.pipeline.repository.model;

public enum Sex {
    FEMALE, MALE
}
