package pro.javatar.pipeline.converter;

import org.mapstruct.Mapper;
import pro.javatar.pipeline.model.UserTO;
import pro.javatar.pipeline.service.model.UserBO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserTOConverter {

    UserTO toUserTO(UserBO bo);

    List<UserTO> toUserTOList(List<UserBO> list);

    UserBO toUserBO(UserTO user);
}
