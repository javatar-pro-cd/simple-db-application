package pro.javatar.pipeline.repository;


import pro.javatar.pipeline.repository.model.UserPO;
import pro.javatar.pipeline.repository.exception.UserNotFoundDBException;

import java.util.List;

public interface UserRepository {
    UserPO findById(Long id) throws UserNotFoundDBException;
    UserPO findByLogin(String login) throws UserNotFoundDBException;
    UserPO findByEmail(String email) throws UserNotFoundDBException;
    UserPO save(UserPO user);
    List<UserPO> findAll();
    void delete(Long userId);
}