FROM openjdk:8-alpine

WORKDIR /service
ENV JAVA_OPTS ""
ENV SERVICE_PARAMS ""
ADD simple-app/target/simple-app.jar /service/
CMD java $JAVA_OPTS -jar simple-app.jar $SERVICE_PARAMS