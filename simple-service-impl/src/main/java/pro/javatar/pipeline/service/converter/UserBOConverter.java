package pro.javatar.pipeline.service.converter;

import org.mapstruct.Mapper;
import pro.javatar.pipeline.repository.model.UserPO;
import pro.javatar.pipeline.service.model.UserBO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserBOConverter {
    UserBO toUserBO(UserPO po);

    UserPO toUserPO(UserBO bo);

    List<UserBO> toUserBOList(List<UserPO> list);
}
